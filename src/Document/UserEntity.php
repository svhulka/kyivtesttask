<?php
namespace App\Document;

use App\AwareTrait\IDAwareInterface;
use App\AwareTrait\IDAwareTrait;
use Swagger\Annotations as SWG;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Class UserEntity
 * @package App\Document
 */
class UserEntity implements IDAwareInterface
{
    use IDAwareTrait;

    /**
     * @var string
     * @SWG\Property(type="string")
     * @Groups({"add", "search"})
     */
    private $username;

    /**
     * @var string
     * @SWG\Property(type="string")
     * @Groups({"add", "search"})
     */
    private $password;

    /**
     * UserEntity constructor.
     * @param string $username
     * @param string $password
     */
    public function __construct(string $username, string $password)
    {
        $this->username = $username;
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }
}
