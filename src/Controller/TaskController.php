<?php
namespace App\Controller;

use App\Document\TaskEntity;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;

/**
 * Class TaskController
 * @package App\Controller
 */
class TaskController extends Controller
{
    /**
     * @SWG\Get(
     *     summary="Search tasks",
     *     description="Search tasks",
     *     @SWG\Parameter(
     *         name="limit",
     *         in="query",
     *         type="integer",
     *         description="Limit",
     *         default="1"
     *     ),
     *     @SWG\Parameter(
     *         name="offset",
     *         in="query",
     *         type="integer",
     *         description="Offset",
     *         default="0"
     *     ),
     *     @SWG\Parameter(
     *         name="sort_field",
     *         in="query",
     *         type="string",
     *         enum={"createdAt"},
     *         description="Sort Field",
     *     ),
     *     @SWG\Parameter(
     *         name="sort_direction",
     *         in="query",
     *         type="string",
     *         enum={"ASC", "DESC"},
     *         description="Sort Direction",
     *     ),
     *     @SWG\Parameter(
     *         name="username",
     *         in="query",
     *         type="string",
     *         description="Username",
     *     ),
     *
     *     @SWG\Response(
     *         response=200,
     *         description="OK",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(
     *                 property="items",
     *                 type="array",
     *                 @SWG\Items(
     *                     @Model(type=TaskEntity::class, groups={"search"})
     *                 )
     *             ),
     *             @SWG\Property(
     *                 property="offset",
     *                 type="integer"
     *             ),
     *             @SWG\Property(
     *                 property="limit",
     *                 type="integer"
     *             ),
     *             @SWG\Property(
     *                 property="countResults",
     *                 type="integer"
     *             )
     *         )
     *     )
     * )
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function search(Request $request): JsonResponse
    {
        $query = $this->get('todo.search_query_builder.task_query_builder')->createFromHttpRequest($request);
        $responseData = $this->get('todo.search_client.task_search_client')->search($query);

        return JsonResponse::create()->setJson(
            $this->get('todo.serializer')->serialize($responseData, 'json')
        );
    }

    /**
     * @SWG\Post(
     *     summary="Create task",
     *     description="Create task",
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         @Model(type=TaskEntity::class, groups={"create"})
     *     ),
     *
     *     @SWG\Response(
     *         response=201,
     *         description="Created",
     *         @Model(type=TaskEntity::class, groups={"search"})
     *     ),
     *
     *     @SWG\Response(
     *         response=400,
     *         description="Validation error",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(
     *                 property="errors",
     *                 type="array",
     *                 @SWG\Items(
     *                     type="object",
     *                     @SWG\Property(
     *                         property="fieldName",
     *                         type="string"
     *                     )
     *                 )
     *             )
     *         )
     *     )
     * )
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function create(Request $request): JsonResponse
    {
        $response = new JsonResponse();
        $responseData = [];
        /**
         * @var TaskEntity $taskEntity
         */
        $taskEntity = $this->get('todo.serializer')
            ->deserialize($request->getContent(), TaskEntity::class, 'json');

        $errors = $this->get('validator')->validate($taskEntity);
        if ($errors->count() > 0) {
            $responseData['errors'] = $errors;
            $response->setStatusCode(Response::HTTP_BAD_REQUEST);
        } else {
            $this->get('todo.repository.task_repository')->save($taskEntity);
            $responseData = $taskEntity;
            $response->setStatusCode(Response::HTTP_CREATED);
        }
        $response->setJson($this->get('todo.serializer')->serialize($responseData, 'json'));

        return $response;
    }

    /**
     * @SWG\Put(
     *     summary="Update task",
     *     description="Update task",
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         @Model(type=TaskEntity::class, groups={"update"})
     *     ),
     *
     *     @SWG\Response(
     *         response=201,
     *         description="Updated",
     *         @Model(type=TaskEntity::class, groups={"search"})
     *     ),
     *
     *     @SWG\Response(
     *         response=400,
     *         description="Validation error",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(
     *                 property="errors",
     *                 type="array",
     *                 @SWG\Items(
     *                     type="object",
     *                     @SWG\Property(
     *                         property="fieldName",
     *                         type="string"
     *                     )
     *                 )
     *             )
     *         )
     *     )
     * )
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function update(Request $request): JsonResponse
    {
        $response = new JsonResponse();
        $taskEntity = $this->get('todo.repository.task_repository')->find($request->attributes->get('task_id'));
        $user = $this->get('todo.repository.user_repository')->findOneBy([]);
        if (!$taskEntity instanceof TaskEntity || $taskEntity->getUser()->getID() != $user->getID()) {
            $response->setStatusCode(Response::HTTP_NOT_FOUND);
            return $response;
        }
        $responseData = [];
        /**
         * @var TaskEntity $taskEntity
         */
        $taskEntity = $this->get('todo.serializer')->deserialize(
            $request->getContent(),
            TaskEntity::class,
            'json',
            ['id' => $request->attributes->get('task_id')]
        );
        $errors = $this->get('validator')->validate($taskEntity);
        if ($errors->count() > 0) {
            $responseData['errors'] = $errors;
            $response->setStatusCode(Response::HTTP_BAD_REQUEST);
        } else {
            $this->get('todo.repository.task_repository')->save($taskEntity);
            $responseData = $taskEntity;
            $response->setStatusCode(Response::HTTP_CREATED);
        }
        $response->setJson($this->get('todo.serializer')->serialize($responseData, 'json'));

        return $response;
    }

    /**
     * @SWG\Delete(
     *     summary="Delete task",
     *     description="Delete task",
     *     @SWG\Parameter(
     *         name="task_id",
     *         in="path",
     *         required=true,
     *         type="string",
     *         description="Task ID",
     *     ),
     *
     *     @SWG\Response(
     *         response=200,
     *         description="Deleted"
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Not Found"
     *     )
     * )
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function delete(Request $request): JsonResponse
    {
        $response = new JsonResponse();

        $taskEntity = $this->get('todo.repository.task_repository')->find($request->attributes->get('task_id'));
        $user = $this->get('todo.repository.user_repository')->findOneBy([]);
        if (!$taskEntity instanceof TaskEntity || $taskEntity->getUser()->getID() != $user->getID()) {
            $response->setStatusCode(Response::HTTP_NOT_FOUND);
            return $response;
        }

        $this->get('todo.repository.task_repository')->delete($taskEntity);
        $response->setJson($this->get('todo.serializer')->serialize([], 'json'));

        return $response;
    }
}
