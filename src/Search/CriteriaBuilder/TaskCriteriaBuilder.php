<?php
namespace App\Search\CriteriaBuilder;

use App\Repository\ToDoRepository;
use App\Search\Query\TaskQuery;
use Doctrine\Common\Collections\Criteria;

/**
 * Class TaskCriteriaBuilder
 * @package App\Search\CriteriaBuilder
 */
class TaskCriteriaBuilder
{
    /**
     * @var ToDoRepository
     */
    private $userRepository;

    /**
     * TaskCriteriaBuilder constructor.
     * @param ToDoRepository $userRepository
     */
    public function __construct(ToDoRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param TaskQuery $query
     * @return Criteria
     */
    public function createFromQuery(TaskQuery $query): Criteria
    {
        $criteria = new Criteria();
        if (!empty($query->getContent())) {
            $criteria->andWhere(Criteria::expr()->contains('content', $query->getContent()));
        }
        if (!empty($query->getUserID())) {
//            $user = $this->userRepository->find($query->getUserID());
//            $criteria->andWhere(Criteria::expr()->contains('user', $user));
        }

        $criteria->setMaxResults($query->getLimit())->setFirstResult($query->getOffset());

        if (!empty($query->getOrderField()) && !empty($query->getOrderDirection())) {
            $criteria->orderBy([$query->getOrderField() => $query->getOrderDirection()]);
        }

        return $criteria;
    }
}
