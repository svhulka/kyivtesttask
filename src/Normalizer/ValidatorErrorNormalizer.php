<?php
namespace App\Normalizer;

use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\ConstraintViolationListInterface;

/**
 * Class ValidatorErrorNormalizer
 * @package App\Normalizer
 */
class ValidatorErrorNormalizer implements NormalizerInterface
{
    /**
     * @inheritdoc
     */
    public function normalize($errors, $format = null, array $context = []) : array
    {
        $result = [];
        /**
         * @var ConstraintViolationList $errors
         */
        foreach ($errors as $error) {
            $result[$error->getPropertyPath()] = $error->getMessage();
        }

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function supportsNormalization($data, $format = null): bool
    {
        return $data instanceof ConstraintViolationListInterface;
    }
}
