<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DefaultController
 * @package App\Controller
 */
class DefaultController extends Controller
{
    /**
     * Index action
     *
     * @return Response
     */
    public function index() : Response
    {
        return $this->render('base.html.twig');
    }
}
