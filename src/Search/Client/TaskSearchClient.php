<?php
namespace App\Search\Client;

use App\Repository\ToDoRepository;
use App\Search\CriteriaBuilder\TaskCriteriaBuilder;
use App\Search\Query\TaskQuery;
use App\Search\Response\SearchResponse;

/**
 * Class TaskSearchClient
 * @package App\Search\Client
 */
class TaskSearchClient
{
    /**
     * @var ToDoRepository
     */
    private $repository;

    /**
     * @var TaskCriteriaBuilder
     */
    private $criteriaBuilder;

    /**
     * TaskSearchClient constructor.
     * @param ToDoRepository $repository
     * @param TaskCriteriaBuilder $criteriaBuilder
     */
    public function __construct(ToDoRepository $repository, TaskCriteriaBuilder $criteriaBuilder)
    {
        $this->repository = $repository;
        $this->criteriaBuilder = $criteriaBuilder;
    }

    /**
     * @param TaskQuery $searchQuery
     * @return SearchResponse
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function search(TaskQuery $searchQuery): SearchResponse
    {
        $criteria = $this->criteriaBuilder->createFromQuery($searchQuery);
        $collection = $this->repository->search($criteria);
        $countResults = $this->repository->countByCriteria($criteria);

        return new SearchResponse($collection->toArray(), $searchQuery, $countResults);
    }
}
