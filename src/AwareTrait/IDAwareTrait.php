<?php
namespace App\AwareTrait;

/**
 * Trait IDAwareTrait
 * @package App\AwareTrait
 */
trait IDAwareTrait
{
    /**
     * @var string
     */
    private $id;

    /**
     * @inheritDoc
     */
    public function getID(): string
    {
        return $this->id;
    }
}
