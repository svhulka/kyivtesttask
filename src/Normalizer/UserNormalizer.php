<?php
namespace App\Normalizer;

use App\Document\UserEntity;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\SerializerAwareInterface;
use Symfony\Component\Serializer\SerializerAwareTrait;

/**
 * Class UserNormalizer
 * @package App\Normalizer
 */
class UserNormalizer implements NormalizerInterface, SerializerAwareInterface
{
    use SerializerAwareTrait;

    /**
     * @inheritdoc
     */
    public function normalize($item, $format = null, array $context = []): array
    {
        /**
         * @var UserEntity $item
         */
        return $this->serializer->normalize([
            'id'       => $item->getID(),
            'username' => $item->getUsername(),
            'password' => $item->getPassword(),
        ], $format, $context);
    }

    /**
     * @inheritdoc
     */
    public function supportsNormalization($data, $format = null): bool
    {
        return ($data instanceof UserEntity);
    }
}
