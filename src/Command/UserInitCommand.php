<?php
namespace App\Command;

use App\Document\UserEntity;
use App\Repository\ToDoRepository;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class UserInitCommand
 * @package App\Command
 */
class UserInitCommand extends ContainerAwareCommand
{
    /**
     * @var ToDoRepository
     */
    private $userRepository;

    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this->setName('todo:user:init')
            ->setDescription('User init command.');
    }

    /**
     * @inheritDoc
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        parent::initialize($input, $output);

        $container = $this->getContainer();
        $this->userRepository = $container->get('todo.repository.user_repository');
    }

    /**
     * @inheritDoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $username = 'admin';
        $password = '123456';
        $user = $this->userRepository->findOneBy(['username' => $username]);
        if (!$user instanceof UserEntity) {
            $this->userRepository->save(new UserEntity($username, md5($password)));
        }
        $output->writeln(
            sprintf(
                "User <info>%s</info> was successfully created.",
                $user->getUsername()
            )
        );
    }
}
