<?php
namespace App\Search\QueryBuilder;

use App\Search\Query\TaskQuery;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class TaskQueryBuilder
 * @package App\Search\QueryBuilder
 */
class TaskQueryBuilder
{
    /**
     * @inheritdoc
     */
    public function createFromHttpRequest(Request $request): TaskQuery
    {
        $query = new TaskQuery();
        $query
            ->setLimit($request->query->getInt('limit', TaskQuery::DEFAULT_LIMIT))
            ->setOffset($request->query->getInt('offset', TaskQuery::DEFAULT_OFFSET))
            ->setOrderField($request->query->get('order_field', TaskQuery::DEFAULT_ORDER_FIELD))
            ->setOrderDirection($request->query->get('order_direction', TaskQuery::DEFAULT_ORDER_DIRECTION))
            ->setContent($request->query->get('content'))
            ->setUserID($request->query->getInt('user_id'));

        return $query;
    }
}
