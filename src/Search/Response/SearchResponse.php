<?php
namespace App\Search\Response;

use App\Search\Query\AbstractQuery;

/**
 * Class SearchResponse
 * @package App\Search\Response
 */
class SearchResponse
{
    /**
     * @var array
     */
    private $items;

    /**
     * @var AbstractQuery
     */
    private $query;

    /**
     * @var int
     */
    private $countResults;

    /**
     * SearchResponse constructor.
     *
     * @param array $items
     * @param AbstractQuery $query
     * @param int $countResults
     */
    public function __construct(array $items, AbstractQuery $query, int $countResults)
    {
        $this->items = $items;
        $this->query = $query;
        $this->countResults = $countResults;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return $this->items;
    }

    /**
     * @return AbstractQuery
     */
    public function getQuery(): AbstractQuery
    {
        return $this->query;
    }

    /**
     * @return int
     */
    public function getCountResults(): int
    {
        return $this->countResults;
    }
}
