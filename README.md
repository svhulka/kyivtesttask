ToDo App
==========

* Run docker command: 
    ```bash
    docker-compose up
    sudo docker exec -it todoapp bash
    ```
* Run user init command: 
    ```bash
    php bin/console todo:user:init
    ```

### API documentation
API documentation can be found [here](http://hostname/api/doc)
