<?php
namespace App\Normalizer;

use App\Document\TaskEntity;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\SerializerAwareInterface;
use Symfony\Component\Serializer\SerializerAwareTrait;

/**
 * Class TaskNormalizer
 * @package App\Normalizer
 */
class TaskNormalizer implements NormalizerInterface, SerializerAwareInterface
{
    use SerializerAwareTrait;

    /**
     * @inheritdoc
     */
    public function normalize($item, $format = null, array $context = []): array
    {
        /**
         * @var TaskEntity $item
         */
        return $this->serializer->normalize([
            'id'        => $item->getID(),
            'content'   => $item->getContent(),
            'completed' => $item->isCompleted(),
            'createdAt' => $item->getCreatedAt()->format('c'),
            'user'      => $item->getUser()
        ], $format, $context);
    }

    /**
     * @inheritdoc
     */
    public function supportsNormalization($data, $format = null): bool
    {
        return ($data instanceof TaskEntity);
    }
}
