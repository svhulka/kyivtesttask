<?php
namespace App\Search\Query;

/**
 * Class TaskQuery
 * @package App\Search\Query
 */
class TaskQuery extends AbstractQuery
{
    /**
     * @var string|null
     */
    private $content;

    /**
     * @var int|null
     */
    private $userID;

    /**
     * @return null|string
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * @param null|string $content
     * @return TaskQuery
     */
    public function setContent(?string $content): TaskQuery
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @return null|int
     */
    public function getUserID(): ?int
    {
        return $this->userID;
    }

    /**
     * @param null|int $userID
     * @return TaskQuery
     */
    public function setUserID(?int $userID): TaskQuery
    {
        $this->userID = $userID;

        return $this;
    }
}
