FROM registry.dev.pbp/base/php7:php71

ENV SYMFONY_ENV prod
ENV PROJECT_PATH /var/www/todoapp
ENV PROJECT_VAR_PATH $PROJECT_PATH/var
ENV LOG_STREAM=$PROJECT_VAR_PATH/stdout

COPY . $PROJECT_PATH

RUN set -x \
    && mkdir -p $PROJECT_VAR_PATH \
    && chmod 777 -R $PROJECT_VAR_PATH \
    && chmod +x $PROJECT_PATH \
    && chown -R www-data:www-data $PROJECT_PATH

WORKDIR $PROJECT_PATH

EXPOSE 9000
RUN touch $LOG_STREAM || mkfifo $LOG_STREAM && chmod 777 $LOG_STREAM
CMD ["/bin/sh", "-c", "php-fpm7.1 -F | tail -f $LOG_STREAM"]