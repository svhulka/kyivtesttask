<?php
namespace App\Normalizer;

use App\Search\Response\SearchResponse;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\SerializerAwareInterface;
use Symfony\Component\Serializer\SerializerAwareTrait;

/**
 * Class SearchResponseNormalizer
 * @package App\Normalizer
 */
class SearchResponseNormalizer implements NormalizerInterface, SerializerAwareInterface
{
    use SerializerAwareTrait;

    /**
     * @inheritdoc
     */
    public function normalize($item, $format = null, array $context = []): array
    {
        /**
         * @var SearchResponse $item
         */
        return $this->serializer->normalize([
            'items'        => $item->toArray(),
            'offset'       => $item->getQuery()->getOffset(),
            'limit'        => $item->getQuery()->getLimit(),
            'countResults' => $item->getCountResults()
        ], $format, $context);
    }

    /**
     * @inheritdoc
     */
    public function supportsNormalization($data, $format = null): bool
    {
        return $data instanceof SearchResponse;
    }
}
