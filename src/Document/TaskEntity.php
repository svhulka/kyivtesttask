<?php
namespace App\Document;

use App\AwareTrait\IDAwareInterface;
use App\AwareTrait\IDAwareTrait;
use Swagger\Annotations as SWG;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Class TaskEntity
 * @package App\Document
 */
class TaskEntity implements IDAwareInterface
{
    use IDAwareTrait;

    /**
     * @var string
     * @SWG\Property(type="string")
     * @Groups({"create", "update", "search"})
     */
    private $content;

    /**
     * @var bool
     * @SWG\Property(type="boolean")
     * @Groups({"create", "update"})
     */
    private $completed = false;

    /**
     * @var UserEntity
     */
    private $user;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * TaskEntity constructor.
     * @param UserEntity $user
     * @param string $content
     */
    public function __construct(UserEntity $user, string $content)
    {
        $this->user = $user;
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @param string $content
     * @return TaskEntity
     */
    public function setContent(string $content): TaskEntity
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @return bool
     */
    public function isCompleted(): bool
    {
        return $this->completed;
    }

    /**
     * @param bool $completed
     * @return TaskEntity
     */
    public function setCompleted(bool $completed): TaskEntity
    {
        $this->completed = $completed;

        return $this;
    }

    /**
     * @return UserEntity
     */
    public function getUser(): UserEntity
    {
        return $this->user;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @return void
     */
    public function onPrePersist(): void
    {
        $this->createdAt = new \DateTime();
    }
}
