<?php
namespace App\AwareTrait;

/**
 * Interface IDAwareInterface
 * @package App\AwareTrait
 */
interface IDAwareInterface
{
    /**
     * @return string
     */
    public function getID(): string;
}
