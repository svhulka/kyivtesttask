<?php
namespace App\Search\Query;

/**
 * Class AbstractQuery
 * @package App\Search\Query
 */
abstract class AbstractQuery
{
    /**
     * @const int
     */
    const DEFAULT_LIMIT = 10;
    
    /**
     * @const int
     */
    const DEFAULT_OFFSET = 0;

    /**
     * @const string
     */
    const DEFAULT_ORDER_FIELD = 'createdAt';

    /**
     * @const string
     */
    const DEFAULT_ORDER_DIRECTION = 'DESC';

    /**
     * @var int | null $limit
     */
    private $limit = self::DEFAULT_LIMIT;

    /**
     * @var int | null $offset
     */
    private $offset = self::DEFAULT_OFFSET;

    /**
     * @var string
     */
    private $orderField = self::DEFAULT_ORDER_FIELD;

    /**
     * @var string
     */
    private $orderDirection = self::DEFAULT_ORDER_DIRECTION;

    /**
     * @param int $offset
     *
     * @return $this
     */
    public function setOffset(int $offset)
    {
        $this->offset = $offset;

        return $this;
    }

    /**
     * @return int
     */
    public function getOffset(): int
    {
        return $this->offset;
    }

    /**
     * @param int $limit
     *
     * @return $this
     */
    public function setLimit(int $limit)
    {
        $this->limit = $limit;

        return $this;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    /**
     * @return string
     */
    public function getOrderField(): string
    {
        return $this->orderField;
    }

    /**
     * @param string $orderField
     *
     * @return $this
     */
    public function setOrderField(string $orderField)
    {
        $this->orderField = $orderField;

        return $this;
    }

    /**
     * @return string
     */
    public function getOrderDirection(): string
    {
        return $this->orderDirection;
    }

    /**
     * @param string $orderDirection
     *
     * @return $this
     */
    public function setOrderDirection(string $orderDirection)
    {
        $this->orderDirection = $orderDirection;

        return $this;
    }
}
