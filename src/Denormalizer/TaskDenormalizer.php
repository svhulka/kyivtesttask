<?php
namespace App\Denormalizer;

use App\Document\TaskEntity;
use App\Repository\ToDoRepository;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

/**
 * Class TaskDenormalizer
 * @package App\Denormalizer
 */
class TaskDenormalizer implements DenormalizerInterface
{
    /**
     * @var ToDoRepository
     */
    private $taskRepository;

    /**
     * @var ToDoRepository
     */
    private $userRepository;

    /**
     * TaskDenormalizer constructor.
     * @param ToDoRepository $taskRepository
     * @param ToDoRepository $userRepository
     */
    public function __construct(ToDoRepository $taskRepository, ToDoRepository $userRepository)
    {
        $this->taskRepository = $taskRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * @inheritdoc
     */
    public function denormalize($data, $class, $format = null, array $context = []): TaskEntity
    {
        $entity = null;
        if (!empty($context['id'])) {
            $entity = $this->taskRepository->find($context['id']);
        }
        if (!$entity instanceof TaskEntity) {
            $entity = new TaskEntity($this->userRepository->findOneBy([]), $data['content'] ?? '');
        } else {
            $entity->setContent($data['content'] ?? '');
        }
        if (isset($data['completed'])) {
            $entity->setCompleted(filter_var($data['completed'], FILTER_VALIDATE_BOOLEAN));
        }

        return $entity;
    }

    /**
     * @inheritdoc
     */
    public function supportsDenormalization($data, $type, $format = null): bool
    {
        return $type === TaskEntity::class;
    }
}
