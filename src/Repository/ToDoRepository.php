<?php
namespace App\Repository;

use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ODM\MongoDB\DocumentRepository;
use Doctrine\ODM\MongoDB\Query\QueryExpressionVisitor;

/**
 * Class ToDoRepository
 * @package App\Repository
 */
class ToDoRepository extends DocumentRepository
{
    /**
     * @param $entity
     * @return void
     */
    public function save($entity): void
    {
        $this->dm->persist($entity);
        $this->dm->flush();
    }

    /**
     * @param $entity
     * @return void
     */
    public function delete($entity): void
    {
        $this->dm->remove($entity);
        $this->dm->flush();
    }

    /**
     * @param Criteria $criteria
     * @return Collection
     */
    public function search(Criteria $criteria): Collection
    {
        return $this->matching($criteria);
    }

    /**
     * @param Criteria $criteria
     * @return int
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function countByCriteria(Criteria $criteria): int
    {
        $visitor = new QueryExpressionVisitor($this->createQueryBuilder());
        $queryBuilder = $this->createQueryBuilder();

        if ($criteria->getWhereExpression() !== null) {
            $expr = $visitor->dispatch($criteria->getWhereExpression());
            $queryBuilder->setQueryArray($expr->getQuery());
        }

        return $queryBuilder->count()->getQuery()->execute();
    }
}
